"""
Phat Simon says...

Created by Michael Aggerholm & Mads Stilling

This game works on a raspberry pi with Touch pHAT.

Remember the sequence of lights, and repeat them, to advance in difficulty.

The light flashes at the same time if you get the wrong answer. They flash
i sequence, if you get it right.

The 'Back' pad/button resets the game.
"""

import touchphat as tp
from random import choice
from time import sleep
import signal

levels = 1
lead = []
follow = []
buttons = ["A", "B", "C", "D"]


def add_step():
    randomstep = choice(buttons)
    lead.append(randomstep)
    print("you have reached level " + str(levels))
    print("added " + str(randomstep) + " to steplist")


def run_steps():
    for i in lead:
        sleep(.2)
        tp.led_on(i)
        sleep(.7)

        tp.led_off(i)


def game():

    global counter
    global levels
    global follow

    # Check if user sequence is correct.
    if counter >= levels and lead == follow:
        win()
        counter = 0
        levels += 1
        follow = []
        sleep(.7)
        add_step()
        run_steps()
    
    # Check if user sequence is wrong.
    elif counter >= levels and lead != follow:
        loose()
        if levels > 1:
            levels -= 1
            lead.pop()
        counter = 0
        follow = []
        sleep(.7)
        run_steps()

        

@tp.on_touch('Back')
def restart_game(event):

    global levels
    global counter
    global lead
    global follow

    print("Restarting level.")
    counter = 0
    follow = []
    run_steps()

    game()


# This function runs, when a button is pressed.
@tp.on_touch(buttons)
def handle_touch(event):
    global counter
    
    print("Button %s pressed!") % event.name
    counter += 1
    follow.append(event.name)
    game()


# What happens when you advance in level.
def win():
    for i in range(3):
            for i in range(1,7):
                    tp.led_on(i)
                    sleep(.02)

            for i in range(1,7):
                    tp.led_off(i)
                    sleep(.02)

# What happens if you get it wrong.
def loose():
    for i in range(3):
       sleep(.3)
       tp.all_on()
       sleep(.3)
       tp.all_off()
    
tp.all_off()
add_step()
run_steps()
counter = 0

signal.pause()
